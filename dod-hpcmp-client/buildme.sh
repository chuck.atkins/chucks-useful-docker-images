#!/bin/bash

docker build --rm \
  -t dod-hpcmp-client \
  --build-arg USER=atkins3 \
  --build-arg UID=$(id -u) \
  --build-arg GID=$(id -g) \
  .
