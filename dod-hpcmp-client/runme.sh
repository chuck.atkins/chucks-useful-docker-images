#!/bin/bash

# Make sure the shared mount point exists
BASE_DIR=$(dirname $(readlink -f ${BASH_SOURCE}))
mkdir -p ${BASE_DIR}/shared
chmod 6755 ${BASE_DIR}/shared

IP=$(ip addr show docker0 | sed -n 's|.*inet \([0-9\.]*\).*|\1|p')
docker run -i \
  --volume="${BASE_DIR}/shared:/mnt/shared:rw" \
  --add-host="${HOSTNAME}:${IP}" --hostname=dod-hpcmp-client \
  -t dod-hpcmp-client \
  /bin/bash
